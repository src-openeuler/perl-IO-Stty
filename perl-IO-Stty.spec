Name:           perl-IO-Stty
Version:        0.04
Release:        1
Summary:        Change and print terminal line settings
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/dist/IO-Stty
Source0:        https://cpan.metacpan.org/authors/id/T/TO/TODDR/IO-Stty-%{version}.tar.gz
BuildArch:      noarch
Patch0:         IO-Stty-0.04-Packed-script-into-rpm.patch
BuildRequires: make
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Run-time
BuildRequires:  perl(POSIX)
# Tests
BuildRequires:  perl(Pod::Coverage) >= 0.18
BuildRequires:  perl(Test::More)

%{?perl_default_filter}

%description
This is the Perl POSIX compliant stty.

%prep
%autosetup -n IO-Stty-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes README
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Fri Mar 29 2024 songliyang <songliyang@kylinos.cn> - 0.04-1
- Package init
